# Gitlab CI for C&#35;

After having spent some time puzzling together fragments of information to get the build configuration I really wanted, I decided to share my CI configuration, in hopes that someone going through a similar situation may benefit from it.    
Admittedly I am not the best at writing documentation.

## Server

The setup I went for is a Windows Server 2012, on which you would have to install:

* [.NET 4.6 Framework](http://go.microsoft.com/fwlink/?LinkId=528259) or whatever framework you're targeting
 * Windows Server 2012 will require Windows update [KB2919355](https://www.microsoft.com/en-us/download/details.aspx?id=42334) which in turn is dependent on [KB2919442](https://www.microsoft.com/en-us/download/details.aspx?id=42153)
* [Microsoft Build Tools 2015](http://www.microsoft.com/en-us/download/details.aspx?id=48159)
* [.NET Framework 4.6 targeting Pack](http://go.microsoft.com/fwlink/?LinkId=528261)

This may not contain all the necessary pieces or SDK's that are shipped with Visual Studio 2015, so you may encounter a `missing .targets files` or other problems building your application.    
Other SDKs may alleviate that problem, or you may simply want to install [Visual Studio](https://www.visualstudio.com/) on the build machine.

* The [Windows 10 Platform SDK](https://dev.windows.com/en-us/downloads/windows-10-sdk)
* The [Azure SDK](http://azure.microsoft.com/en-us/downloads/)

Credit for most of this information: [jessehouwing](http://stackoverflow.com/users/736079/jessehouwing) for his [StackOverflow answer](http://stackoverflow.com/a/32070575)

## Variables

Project name and build configuration are the only variables that are likely to alternate between projects, so they have been made easily modifiable.

```yml
variables:
  PROJECT_NAME: "TestProject"
  CONFIG_TYPE: "Release"
```

Before starting the build of the solution you must restore the nuget packages. To run this command you must have [NuGet.exe](https://dist.nuget.org/) at your disposal.    
If you don't want to add NuGet to PATH, you can replace that line with `C:\path\to\nuget.exe restore "%PROJECT_NAME%.sln"`

```yml
before_script:
  - echo "Starting build for %PROJECT_NAME%"
  - echo "Restoring NuGet Packages..."
  - nuget restore "%PROJECT_NAME%.sln"
```

We must assure ourselves that the commit will successfully build before we want to deploy the code. For that we use the build stage.    

```yml
build:
  stage: build
  script:
  - echo "Verify build"
  - '"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" /consoleloggerparameters:ErrorsOnly /maxcpucount /nologo /property:Configuration=%CONFIG_TYPE% /verbosity:quiet "%PROJECT_NAME%.sln"'
```

If the build is successful we can continue on to publishing / deploying our code.   
This method, that I used, has an external dependency on a _publish.pubxml_ file.

* Untracked: We keep this false, as we don't want any files in the artifacts that the project didn't build itself.
* Paths: All the artifacts that are to be deployed get added to the project zip file.

```yml
production:
    stage: deploy
    only:
      - master
    script:
    - echo "Deploying code and submitting artifacts"
    - '"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe" /consoleloggerparameters:ErrorsOnly /verbosity:quiet /maxcpucount /nologo /property:Configuration=%CONFIG_TYPE% /p:DeployOnBuild=true /p:PublishProfile="C:\temp\PublishProfiles\%PROJECT_NAME%\publish.pubxml" "%PROJECT_NAME%.sln"'
    artifacts:
        name: "%CI_PROJECT_NAME%_%CI_BUILD_ID%-%CI_BUILD_REF_NAME%"
        untracked: false
        paths:
        - "%PROJECT_NAME%/obj/%CONFIG_TYPE%/Package/PackageTmp/"
    dependencies:
        - build
```

### publish.pubxml

```xml
<?xml version="1.0" encoding="utf-8"?>
<!--
This file is used by the publish/package process of your Web project. You can customize the behavior of this process
by editing this MSBuild file. In order to learn more about this please visit http://go.microsoft.com/fwlink/?LinkID=208121. 
-->
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <WebPublishMethod>FileSystem</WebPublishMethod>
    <LastUsedBuildConfiguration>Release</LastUsedBuildConfiguration>
    <LastUsedPlatform>Any CPU</LastUsedPlatform>
    <SiteUrlToLaunchAfterPublish />
    <LaunchSiteAfterPublish>True</LaunchSiteAfterPublish>
    <ExcludeApp_Data>False</ExcludeApp_Data>
    <publishUrl>C:\path\to\builds\ProjectName</publishUrl>
    <DeleteExistingFiles>False</DeleteExistingFiles>
  </PropertyGroup>
</Project>
```